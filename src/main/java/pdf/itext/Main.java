package pdf.itext;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

public class Main {

    public static void main(String[] args) throws FileNotFoundException, DocumentException {

        Document document = new Document(PageSize.A4, 50, 50, 50, 50);
        PdfWriter.getInstance(document, new FileOutputStream("resume.pdf"));
        document.open();

        //content
        addDocumentData(document);
        addHeadingPage(document);
        createContent(document);

        document.close();
    }

    private static void addDocumentData(Document document) {
        document.addTitle("Resume");
        document.addAuthor("Szymon Tyrka");
        document.addCreator("Szymon Tyrka");
    }

    private static void addHeadingPage(Document document) throws DocumentException {

        Font headingFont = new Font(Font.FontFamily.HELVETICA, 34, Font.BOLD);

        Paragraph heading = new Paragraph("Resume", headingFont);
        heading.setAlignment(Element.ALIGN_CENTER);
        document.add(heading);
        addEmptyLine(document, 4);

    }

    private static void addEmptyLine(Document document, int number) throws DocumentException {
        for (int i = 0; i < number; i++) {
            document.add(new Paragraph(" "));
        }
    }

    private static void createContent(Document document) throws DocumentException {
        String education = "2018 - 2022   Colegium Tarnoviense\n" +
                            "2015 - 2018   I High School in Tarnow";

        String summary = "I haven't had professional experience yet, but I have mastered " +
                "some of the programming languages and the technologies used in them quite well. " +
                "I have already done some projects based mainly on java language, which are available " +
                "on my remote repository. Together with my colleagues from my studies, we have used the " +
                "version control system many times, so I am familiar with working in this system. " +
                "I am also interested in creating mobile applications on android. I use java and React Native for this." +
                " I have knowledge of the Spring library and databases. I like to work in a group and solve problems together.";

        PdfPTable table = new PdfPTable(2);

        table.setWidthPercentage(100);
        table.addCell(createCell("First Name", Element.ALIGN_LEFT ));
        table.addCell(createCell("Szymon", Element.ALIGN_LEFT ));
        table.addCell(createCell("Last name", Element.ALIGN_LEFT ));
        table.addCell(createCell("Tyrka", Element.ALIGN_LEFT ));
        table.addCell(createCell("Profession", Element.ALIGN_LEFT ));
        table.addCell(createCell("Student", Element.ALIGN_LEFT ));
        table.addCell(createCell("Education", Element.ALIGN_LEFT ));
        table.addCell(createCell(education, Element.ALIGN_LEFT ));
        table.addCell(createCell("Summary", Element.ALIGN_LEFT ));
        table.addCell(createCell(summary, Element.ALIGN_LEFT ));

        document.add(table);

    }

    private static PdfPCell createCell(String content, int alignment) {

        Font font = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.NORMAL);

        PdfPCell cell = new PdfPCell(new Phrase(content, font));
        cell.setHorizontalAlignment(alignment);
        cell.setPadding(6);
        cell.setUseAscender(true);
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);

        return cell;
    }
}
